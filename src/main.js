// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App.vue'
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'
import router from './router'
import '../static/base/base.css'
import '../static/base/base'
import './assets/font/iconfont.css'
import axios from 'axios'
import VueLazyLoad from 'vue-lazyload'
Vue.use(VueLazyLoad);
axios.defaults.baseURL = '/api';
axios.defaults.headers['apikey'] = 'test';
// axios.defaults.baseURL = 'http://news.izxcs.com'
// axios.defaults.baseURL = '192.168.3.185'
Vue.prototype.$axios = axios;
Vue.use(MintUI);
Vue.config.productionTip = false;
import VuePreview from 'vue-preview'
Vue.use(VuePreview);
import wcSwiper from 'wc-swiper'
import 'wc-swiper/style.css'
Vue.use(wcSwiper);
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
});
