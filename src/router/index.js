import Vue from 'vue'
import Router from 'vue-router'
import home from '../components/home/home.vue'
import video from '../components/video/video.vue'
import newsInfo from '../components/home/newsInfo.vue'
import videoInfo from '../components/video/videoInfo.vue'
import login from '../components/login/login.vue'
import logining from '../components/login/logining.vue'
import register from '../components/login/register.vue'
import mine from '../components/mine/mine.vue'
import resetPassword_step1 from '../components/login/resetPassword_step1.vue'
import resetPassword_step2 from '../components/login/resetPassword_step2.vue'
import mineActive from '../components/mine/mine-active.vue'
import mineCollectionAndHistory from '../components/mine/mine-CollectionAndHistory'
import mineAttentionAndFans from '../components/mine/mine-attentionAndFans'
import mineEwCode from '../components/mine/mine-ewCode'
import search from '../components/search/search'
import pictures from '../components/common/pictures'
import care from '../components/care/care'
import addCare from '../components/care/addCare'
import friendUpdates from '../components/care/friendUpdates'
import authorPage from '../components/otherAuthor/otherAuthor'
import taAttentionAndFans from '../components/otherAuthor/ta-attentionAndFans'
import topicHome from '../components/topic/topicHome'
import topicInfo from '../components/topic/topicInfo'
import topicDetails from '../components/topic/topicDetails'
// import Navigator from '../components/common/Navigator'
// Vue.component(Navigator.name, Navigator);
Vue.use(Router);
// Vue.config.productionTip = false
export default new Router({
  linkActiveClass: 'tabbar-active',
  // mode: 'history',
  routes: [
    {path: '/', redirect: '/home'},//路由重定向
    {
      path: '/home',
      component: home,
      children:[
        {path: 'newsInfo/:articleId', component: newsInfo,name:'newsInfo'},
        {path: 'pictures/:articleId', component: pictures,name:'pictures'},
        {path: 'videoInfo/:videoId', component: videoInfo,name:'videoInfo'},
        {
          path: '/newsSearch',component: search,name:'newsSearch',
          children:[
            {path: 'videoInfo/:videoId', component:videoInfo,name:'videoInfo'},
            {path: 'newsInfo/:articleId', component:newsInfo,name:'newsInfo'},
            {path: 'pictures/:articleId', component:pictures,name:'pictures'},
          ]
        },
      ]
    },
    {
      path: '/video',
      component: video,
      children:[
        {path: 'videoInfo/:videoId', component: videoInfo,name:'videoInfo'},
        {
          path: '/videoSearch', component: search,name:'videoSearch',
          children:[
            {path: 'videoInfo/:videoId', component:videoInfo,name:'videoInfo'},
            {path: 'newsInfo/:articleId', component:newsInfo,name:'newsInfo'},
            {path: 'pictures/:articleId', component:pictures,name:'pictures'},
          ]
        },
      ]

    },
    {path: '/login', component: login},
    {path: '/login/logining/:to', component: logining},
    {path: '/login/register/toRegister', component: register},
    {
      path: '/mine',
      component: mine,
      meta: {
        requiredAuth: true
      },
      // beforeEnter: (to, from, next) => {
      //   if (to.meta.requiredAuth) {
      //     if (sessionStorage.getItem('userId')) {
      //       next()
      //     } else {
      //       next({
      //         path: '/login?to=mine'
      //       })
      //     }
      //   } else {
      //     next()
      //   }
      // },
    },
    {path: '/login/resetPassword/resetPassword_step1', component: resetPassword_step1},
    {path: '/login/resetPassword/resetPassword_step1/resetPassword_step2', component: resetPassword_step2},
    {
      path: '/mine/mine-active', component: mineActive,name:'mine-active',
      children:[
        {path: 'newsInfo/:articleId', component: newsInfo,name:'newsInfo'},
        {path: 'pictures/:articleId', component: pictures,name:'pictures'},
        {path: 'videoInfo/:videoId', component: videoInfo,name:'videoInfo'},
        {path: 'authorPage/:authorId', component: authorPage,name:'authorPage'},
      ]
    },
    {
      path: '/mine/mine-CollectionAndHistory/:cate', component: mineCollectionAndHistory,
      children:[
        {path: '/mine/mine-CollectionAndHistory/newsInfo/:articleId', component: newsInfo,name:'newsInfo'},
        {path: '/mine/mine-CollectionAndHistory/pictures/:articleId', component: pictures,name:'pictures'},
        {path: '/mine/mine-CollectionAndHistory/videoInfo/:videoId', component: videoInfo,name:'videoInfo'},
      ]
    },
    {path: '/mine/mine-attentionAndFans/:cate', component: mineAttentionAndFans},
    {path: '/mine/min-ewCode/:enshrinedNum', component: mineEwCode},
    {
      path: '/care',
      component: care,
      meta: {
        requiredAuth: true
      },
      // beforeEnter: (to, from, next) => {
      //   if (to.meta.requiredAuth) {
      //     if (sessionStorage.getItem('userId')) {
      //       next()
      //     } else {
      //       next({
      //         path: '/login?to=care'
      //       })
      //     }
      //   } else {
      //     next()
      //   }
      // },
    },
    {path: '/care/addCare', component: addCare},
    {
      path: '/care/friendUpdates', component: friendUpdates,name:friendUpdates,
      children:[
        {path: 'newsInfo/:articleId', component: newsInfo,name:'newsInfo'},
        {path: 'pictures/:articleId', component: pictures,name:'pictures'},
        {path: 'videoInfo/:videoId', component: videoInfo,name:'videoInfo'},
        {path: 'authorPage/:authorId', component: authorPage,name:'authorPage'},
      ]
    },
    // {path: '/videoInfo/:videoId', component:videoInfo},
    // {path: '/newsInfo/:articleId', component:newsInfo},
    {
      path:'/author/authorPage/homePage/:authorId',component:authorPage,
      children:[
        {path: '/author/authorPage/homePage/newsInfo/:articleId', component: newsInfo,name:'newsInfo'},
        {path: '/author/authorPage/homePage/pictures/:articleId', component: pictures,name:'pictures'},
        {path: '/author/authorPage/homePage/videoInfo/:videoId', component: videoInfo,name:'videoInfo'},
        {path: '/author/authorPage/homePage/ta-attentionAndFans/:cate/:authorId', component: taAttentionAndFans,name:'taAttentionAndFans'},
      ]
    },
    {
      path:'/topicHome',component:topicHome,
      children:[
        {
          path: '/topicSearch',component: search,name:'topicSearch',
          children:[
            {path: 'videoInfo/:videoId', component:videoInfo,name:'videoInfo'},
            {path: 'newsInfo/:articleId', component:newsInfo,name:'newsInfo'},
            {path: 'pictures/:articleId', component:pictures,name:'pictures'},
          ]
        },
      ]
    },
    {path:'/topicHome/topicInfo/:scCmsSpecialId',component:topicInfo,},
    {path: '/topicHome/topicInfo/newsInfo/:articleId', component: newsInfo,name:'newsInfo'},
    {path: '/topicHome/topicInfo/pictures/:articleId', component: pictures,name:'pictures'},
    {path: '/topicHome/topicInfo/videoInfo/:videoId', component: videoInfo,name:'videoInfo'},
    {path: '/topicHome/topicInfo/topicDetails/:scCmsSpecialId', component: topicDetails,name:'topicDetails'},

  ],
})
